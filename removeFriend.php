<?php
header('Content-Type: application/json; charset=utf-8;');
require("database.php");
if (logged()) {
    removeFriend($_GET['id']);
    header("Location: friends.php");
}
?>