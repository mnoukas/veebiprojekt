<?php
header('Content-Type: application/json; charset=utf-8');
require("database.php");
if (logged()) {
    updateUserData();
    header("Location: edit.php");
}
?>