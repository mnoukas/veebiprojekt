<?php 
    require("database.php") 
?>

<!doctype HTML>
<html>
	<head>
    	<title> Veebiprojekt </title>
    	<meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="atribuudid/stiil.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width = device-width, initial-scale = 1">
	</head>
	<body style="background-image: url(images/Sun.jpg)">
    <?php if (logged()) : ?>
    <div class="container">
        <div class="page-header">
            <h1>Tere tulemast <?=$_SESSION['Kasutajanimi']?>!</h1>
        </div>
        <div class="jumbotron">
            <h2> Otsi uusi tutvusi! </h2>
                <div class="btn-group-g" style="text-align: center; margin-top: 28px">
                    <a class="btn btn-success" id="btn_Pealeht" href="login_success.php"><span class="glyphicon glyphicon-home"></span> Pealehele</a>
                    <a class="btn btn-warning" id="btn_Friends" href="friends.php"><span class="glyphicon glyphicon-user"></span> Sinu sõbrad </a>
                    <a class="btn btn-info" id="btn_Edit" href="edit.php"><span class="glyphicon glyphicon-pencil"></span> Muuda profiili </a>
                    <a class="btn btn-danger" id="btn_LogOut" href="logout.php"><span class="glyphicon glyphicon-warning-sign"></span> Logi välja </a>
                </div>
            </div>
        <div style="overflow: auto">
        <table class="table table-responsive table-bordered table-striped table-hover">
            <h2 id="pealkiri">Kasutajad</h2>
            <thead>
            <tr class="active">
                <th class="text-center"> Sugu </th>
                <th class="text-center"> Vanus </th>
                <th class="text-center"> Eesnimi </th>
                <th class="text-center"> Asukoht </th>
                <th class="text-center"> Taotlus</th>
            </tr>
            </thead>
            <tbody>
            <tr class="success">
                <td> Naine </td>
                <td> 21 </td>
                <td> Helen </td>
                <td> Tartu </td>
                <td><button class="btn btn-primary"><span class="glyphicon glyphicon-star"></span> Saada sõbrataotlus </button></td>
            </tr>
        </table>
        </div>
        </div>
    <?php else : ?>
        <?php header("Location: logimine.php"); ?>
    <?php endif ?>
</body>
</html>
	