<?php
header('Content-Type: application/json; charset=utf-8');
require("database.php");
if (logged()) {
    sendRequest();
    header("Location: login_success.php");
}
?>