<?php
header('Content-Type: application/json; charset=utf-8;');
require("database.php");
if (logged()) {
    changeStatusConfirm();
    header("Location: friends.php");
}
?>