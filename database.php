<?php

header('Content-Type: charset=utf-8');

// Databaasiga ühendamiseks vajalikud muutujad

define("HOST", "localhost");
define("USER", "test");
define("PASS", "t3st3r123");
define("DB", "test");

/* Funktsioon, mis üritab saada andmebaasiga ühendust */
function getDatabaseConnection()
{
    $con = mysqli_connect(HOST, USER, PASS, DB);
    
    mysqli_query($con, 'SET CHARACTER TO UTF-8');
    if ($con->connect_error) {
        die("Ebaõnnestus: " . $con->connect_error);
    } else {
        return $con;
    }
}

/*
 * Funktsioon kasutaja registreerimiseks ning tekitamiseks tabelisse mnoukas__kasutajad,
 * samuti hashitakse kasutaja parool
 */
function register($username, $password, $passwordConf)
{
    $con = getDatabaseConnection();
    
    $hash  = password_hash($password, PASSWORD_DEFAULT);
    $sql   = "SELECT count(*) FROM mnoukas__kasutajad WHERE Kasutajanimi = ?;";
    $query = $con->prepare($sql);
    $query->bind_param('s', $username);
    $query->execute();
    $result = $query->get_result();
    $row    = $result->fetch_row();
    if ($row[0] != 0) {
        return "userExists";
    } else {
        if (($password == $passwordConf) && (strlen($username) > 8 && strlen($password) > 8)) {
            $sql   = "INSERT INTO mnoukas__kasutajad (Kasutajanimi, Parool) VALUES (?, ?);";
            $query = $con->prepare($sql);
            $query->bind_param('ss', $username, $hash);
            $query->execute();
            $con->close();
            return "done";
        } else {
            return "passwordMatch";
        }
    }
}

/*
 * Funktsioon, mis kontrollib, kas selline kasutaja ja parool eksisteerivad,
 * / kui eksisteerivad, siis logi kasutaja sisse
 */
function login($username, $password)
{
    startSession();
    $con = getDatabaseConnection();
    
    $sql   = "SELECT id, Kasutajanimi, Parool FROM mnoukas__kasutajad WHERE Kasutajanimi = ? LIMIT 1;";
    $query = $con->prepare($sql);
    $query->bind_param('s', $username);
    $query->execute();
    $result = $query->get_result();
    $row    = $result->fetch_assoc();
    
    $con->close();
    
    if (isset($row) && password_verify($password, $row['Parool'])) {
        $_SESSION['id']           = preg_replace("/[^0-9]+/", "", $row['id']);
        $_SESSION['Kasutajanimi'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["Kasutajanimi"]);
        $_SESSION['Parool']       = $row["Parool"];
        return true;
    } else {
        return "wrongCredentials";
    }
}

/* Sisselogimisel tulnud vea näitamine/selgitamine kasutajale */
function getAlert($getData)
{
    if (isset($getData['alert'])) {
        switch ($getData['alert']) {
            case "wrongCredentials":
                $alert = "Vale kasutajanimi või parool.";
                break;
            case "passwordMatch":
                $alert = "Paroolid ei klapi või on kasutajanimi/parool liiga lühike, ehk alla 8 tähemärgi.";
                break;
            case "userExists":
                $alert = "Selline kasutaja on juba olemas!.";
                break;
            default:
                $alert = "Midagi läks tuksi.";
                break;
        }
        return $alert;
    } else {
        return false;
    }
}

/* Funktsioon, mis tagastab success alerdi, kui uus kasutaja saab loodud mnoukas__kasutajad tabelisse */
function getSuccess($getData)
{
    if (isset($getData['success'])) {
        switch ($getData['success']) {
            case "done":
                $alert = "Kasutaja registreeritud.";
                break;
            default:
                $alert = "Midagi läks tuksi.";
                break;
        }
        return $alert;
    } else {
        return false;
    }
}

/* Funktsioon, mis tekitab kõigepealt sessiooni ning kontrollib kas kasutaja on ikka sisselogitud */
function logged()
{
    startSession();
    if (isset($_SESSION['id'], $_SESSION['Kasutajanimi'], $_SESSION['Parool'])) {
        $id       = $_SESSION['id'];
        $username = $_SESSION['Kasutajanimi'];
        $password = $_SESSION['Parool'];
        
        $con = getDatabaseConnection();
        
        $sql   = "SELECT Parool FROM mnoukas__kasutajad WHERE id = ? LIMIT 1;";
        $query = $con->prepare($sql);
        $query->bind_param('i', $id);
        $query->execute();
        
        $result = $query->get_result();
        $row    = $result->fetch_assoc();
        if (isset($row)) {
            if ($password == $row['Parool']) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/* Sessiooni tegemise funktsioon */
function startSession()
{
    $session_name = 'sessaeg';
    $secure       = false;
    $httponly     = true;
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    session_name($session_name);
    session_start();
    session_regenerate_id(true);
}

/* Funktsioon, mille töölepanekul hävitatakse sessioon ning kasutaja peab uuesti sisse logima */
function logout()
{
    startSession();
    $_SESSION = array();
    $params   = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
    session_destroy();
}

/*
 * Funktsioon, mis leiab kasutajajaoks kõik kasutajad, kellega ta veel sõber pole
 * / või pole sõbrataotlust saatnud, ehk siis niiöelda "vabad sõbrad, keda lisada"
 */
function findPeople()
{
    $con = getDatabaseConnection();
    
    $sql   = "SELECT Id, Kasutajanimi, Sugu, Vanus, Eesnimi, Asukoht FROM mnoukas__kasutajad WHERE id <> ? AND id NOT IN (SELECT UserTwoID FROM mnoukas__friends WHERE UserOneID = ?) AND id NOT IN (SELECT UserOneID FROM mnoukas__friends WHERE UserTwoID = ?);";
    $query = $con->prepare($sql);
    $query->bind_param('iii', $_SESSION['id'], $_SESSION['id'], $_SESSION['id']);
    $query->execute();
    
    $result = $query->get_result();
    $rows   = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    return $rows;
}

/* Funktsioon, mis tagastab hetkekasutaja andmed */
function getUserData()
{
    $con = getDatabaseConnection();
    
    $sql   = "SELECT Sugu, Vanus, Eesnimi, Asukoht FROM mnoukas__kasutajad WHERE id = ? LIMIT 1;";
    $query = $con->prepare($sql);
    $query->bind_param('i', $_SESSION['id']);
    $query->execute();
    
    $result = $query->get_result();
    $row    = $result->fetch_assoc();
    return $row;
}

/* Funktsioon, mis siis vastavalat kasutajainputile väljades, muudab kasutaja andmeid */
function updateUserData()
{
    $con = getDatabaseConnection();
    
    $sql   = "UPDATE mnoukas__kasutajad SET Sugu = ?, Vanus = ?, Eesnimi = ?, Asukoht = ? WHERE id = ?;";
    $query = $con->prepare($sql);
    $query->bind_param('sissi', $_POST['edit_Sugu'], intval($_POST['edit_Vanus']), $_POST['edit_Nimi'], $_POST['edit_Asukoht'], $_SESSION['id']);
    $query->execute();
}

/*
 * Funktsioon, mis tekitab andmebaasi tabelisse mnoukas__friends uue välja, kus UserOne-iks on saatja
 * / UserTwo-ks on saaja ning status 1 tähendab seda, et sõbrakutse on ootel
 */
function sendRequest()
{
    $con = getDatabaseConnection();
    
    $userOne = $_SESSION['id'];
    $userTwo = $_POST["accept"];
    $status  = 1;
    
    $sql   = "INSERT INTO mnoukas__friends(UserOneID, UserTwoID, Status) VALUES (?, ?, ?);";
    $query = $con->prepare($sql);
    $query->bind_param('iii', $userOne, $userTwo, $status);
    $query->execute();
}

/* Funktsioon, mis tagastab tabeli nende kasutajatega, kes on sõbrataotluse Sulle saatnud */
function getFriends()
{
    $con = getDatabaseConnection();
    
    $status = 1;
    $sql    = "SELECT Kasutajanimi, mnoukas__kasutajad.id FROM mnoukas__kasutajad INNER JOIN mnoukas__friends ON mnoukas__kasutajad.id = mnoukas__friends.UserOneID WHERE mnoukas__friends.UserTwoID = ? AND mnoukas__friends.Status = ?;";
    $query  = $con->prepare($sql);
    $query->bind_param('ii', $_SESSION['id'], $status);
    $query->execute();
    
    $result = $query->get_result();
    $rows   = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    return $rows;
}

/* Funktsioon, mis võtab vastu sõbrakutse ning muudab Statuse mnoukas__friends tabelis 2-ks */
function changeStatusConfirm()
{
    $con = getDatabaseConnection();
    
    $status  = 2;
    $confirm = $_POST["confirm"];
    $sql     = "UPDATE mnoukas__friends SET Status = ? WHERE UserOneID = ? AND UserTwoID = ?;";
    $query   = $con->prepare($sql);
    $query->bind_param('iii', $status, $confirm, $_SESSION['id']);
    $query->execute();
}

/* Funktsioon, mis eirab/lükkab tagasi sõbrakutse ning kustutab sõbrakutse rea mnoukas__friends tabelist */
function changeStatusDeny()
{
    $con = getDatabaseConnection();
    
    $deny  = $_POST["deny"];
    $sql   = "DELETE FROM mnoukas__friends WHERE (UserOneID = ? AND UserTwoID = ?);";
    $query = $con->prepare($sql);
    $query->bind_param('ii', $deny, $_SESSION['id']);
    $query->execute();
}

/* Funktsioon, mis näitab kasutajaid, kellega Sinul suhe ehk status on 2, ehk siis "sõber" */
function showFriends($id = null)
{
    $con = getDatabaseConnection();
    
    $id    = $id == null ? $_SESSION['id'] : $id;
    $sql   = "SELECT id, Kasutajanimi, Sugu, Vanus, Eesnimi, Asukoht FROM mnoukas__kasutajad WHERE id <> ? AND id IN (SELECT UserTwoID FROM mnoukas__friends WHERE UserOneID = ? AND Status = 2) OR id IN (SELECT UserOneID FROM mnoukas__friends WHERE UserTwoID = ? AND Status = 2);";
    $query = $con->prepare($sql);
    $query->bind_param('iii', $id, $id, $id);
    $query->execute();
    
    $result = $query->get_result();
    $rows   = array();
    while ($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    return $rows;
}

/* Funktsioon, mis eemaldab sõbrasuhte tabelist mnoukas__friends */
function removeFriend($id)
{
    $con = getDatabaseConnection();
    
    $sql   = "DELETE FROM mnoukas__friends WHERE (UserOneID = ? AND UserTwoID = ?) || (UserOneID = ? AND UserTwoID = ?);";
    $query = $con->prepare($sql);
    $query->bind_param('iiii', $id, $_SESSION['id'], $_SESSION['id'], $id);
    $query->execute();
}

?>
=======
	header('Content-Type: text/html; charset=utf-8');
	// Databaasiga ühendamiseks vajalikud muutujad
	define("HOST", "localhost");
	define("USER", "test");
	define("PASS", "t3st3r123");
	define("DB", "test");
	
function getDatabaseConnection () {
	$con = mysqli_connect(HOST, USER, PASS, DB);
	mysqli_query($con, 'SET CHARACTER TO UTF-8');
	if ($con->connect_error) {
		die("Connection failed: " . $con->connect_error);
	} else {
    return $con;
  }
}

function register ($username, $password, $passwordConf)
{
	$con = getDatabaseConnection();
		if ($password == $passwordConf) {
			$sql = "INSERT INTO mnoukas__kasutajad (Kasutajanimi, Parool) VALUES (?, ?);";
			$query = $con->prepare($sql);
			$query->bind_param('ss', $username, $password);
			$query->execute();
			$con->close();
			header("Location: logimine.php");
		} else {
			header("Location: registreerumine.php");
			echo("Valed andmed!");
		}
	}
function login ($username, $password) {
  startSession();
	$con = getDatabaseConnection();
   
	$sql = "SELECT id, Kasutajanimi, Parool FROM mnoukas__kasutajad WHERE Kasutajanimi = ? LIMIT 1;";
	$query = $con->prepare($sql);
	$query->bind_param('s', $username);
	$query->execute();
	$result = $query->get_result();
	$row = $result->fetch_assoc();
  
  $con->close();
  
	if (isset($row) && $row["Parool"] = $password) {
		$_SESSION['id'] = preg_replace("/[^0-9]+/", "", $row['id']);
		$_SESSION['Kasutajanimi'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["Kasutajanimi"]);
		$_SESSION['Parool'] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $row["Parool"]);
		return true;
	}
	else {
		echo('<div class="alert alert-danger"> SHIT! </div>');
		echo('<h1> FUCK </h1>');
		return false;
	}
}

function displayUser () {
	$con = getDatabaseConnection();

	$gender = $_SESSION['Sugu'];
	$age = $_SESSION['Vanus'];
	$firstname = $_SESSION['Eesnimi'];
	$location = $_SESSION['Asukoht'];

	$sql = "SELECT Sugu, Vanus, Eesnimi, Asukoht FROM mnoukas__kasutajad;";


}
function logged () {
  startSession();
  if (isset($_SESSION['id'], $_SESSION['Kasutajanimi'], $_SESSION['Parool'])) {
    $id = $_SESSION['id'];
    $username = $_SESSION['Kasutajanimi'];
    $password = $_SESSION['Parool'];
    
		$con = getDatabaseConnection();
    
		$sql = "SELECT Parool FROM mnoukas__kasutajad WHERE id = ? LIMIT 1;";
		$query = $con->prepare($sql);
		$query->bind_param('i', $id);
		$query->execute();
		
		$result = $query->get_result();
		$row = $result->fetch_assoc();
		if (isset($row)) {
			if ($password == $row['Parool']){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function startSession () {
	$session_name = 'sessaeg';
	$secure = false;
	$httponly = true;
	$cookieParams = session_get_cookie_params();
	session_set_cookie_params($cookieParams["lifetime"],
		$cookieParams["path"],
		$cookieParams["domain"],
		$secure,
		$httponly);
	session_name($session_name);
	session_start();
	session_regenerate_id(true);
}

function logout () {
		startSession();
		$_SESSION = array();
		$params = session_get_cookie_params();
		setcookie(session_name(),
			'', time() - 42000,
			$params["path"],
			$params["domain"],
			$params["secure"],
			$params["httponly"]);
		session_destroy();
	}

function findPeople () {

}

function editProfile () {
	
}
?>
>>>>>>> 45cbde5d9de68c2bccadd7751e6e387c2bb62e7e
